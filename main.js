new Vue({
    el: '#app',
    data: {
        twitchChannels: [],
        userList: [
            "ESL_SC2",
            "OgamingSC2",
            "cretetion",
            "freecodecamp",
            "storbeck",
            "habathcx",
            "RobotCaleb",
            "noobs2ninjas",
            "comster404",
            "brunofin",
            "evilarthas",
            "nickbunyun"
        ]
    },

    ready: function () {
        this.fetchTwitchChannels(this.userList);
        this.twitchChannels.sort(function (a, b) {
            return a.sort - b.sort
        });
        this.twitchStreamers = this.twitchChannels;
    },

    methods: {
        fetchTwitchChannels: function (userList) {
            var self = this;
            var channelList = [];
            var promises = userList.map(function (user) {
                return axios.get('https://api.twitch.tv/kraken/channels/' + user, {
                    headers: {
                        'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
                    },
                    validateStatus: function (status) {
                        return (status >= 200 && status < 300) || status === 404
                    }
                })
            });
            Promise.all(promises)
                .then(function (twitchData) {
                    console.log(twitchData)
                    return twitchData.map(function (item) {
                        var twitchChannel = {};
                        var responseBody = item.data;
                        if (item.status === 200) {
                            twitchChannel.logo = responseBody.logo;
                            twitchChannel.name = responseBody.name;
                            twitchChannel.url = responseBody.url;
                            twitchChannel.statusText = null;
                            twitchChannel.class = 'panel panel-info';
                            twitchChannel.sort = 0;
                        } else {
                            var temp = item.request.responseURL.split('/');
                            twitchChannel.name = temp[temp.length - 1];
                            twitchChannel.url = '';
                            twitchChannel.statusText = 'Channel Not Found';
                            twitchChannel.class = 'panel panel-danger';
                            twitchChannel.sort = 2;
                        }
                        twitchChannel.ok = item.ok;
                        return twitchChannel;
                    });
                })
                .then(function (resTwichChannels) {
                    channelList = resTwichChannels;
                    return Promise.all(
                        resTwichChannels.map(function (item) {
                            return axios.get('https://api.twitch.tv/kraken/streams/' + item.name, {
                                headers: {
                                    'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
                                },
                                validateStatus: function (status) {
                                    return (status >= 200 && status < 300) || status === 404
                                }
                            })
                        })
                    )
                })

                .then(function (resTwitchStreams) {

                    console.log(channelList)
                    console.log(resTwitchStreams);

                    const _twitchChannels = channelList.map(function (channelItem) {
                        resTwitchStreams.forEach(function (streamItem) {

                            const stream = streamItem.data.stream
                            const temp = streamItem.request.responseURL.split('/')
                            const streamName = temp[temp.length - 1]

                            if(channelItem.name === streamName){
                                if(stream) {
                                    channelItem.statusText = stream.channel.status
                                    channelItem.class = 'panel panel-success'
                                } else {
                                    if(!channelItem.statusText) {
                                        channelItem.statusText = 'Offline'
                                        channelItem.sort = 1
                                    }
                                }
                            }
                            
                            
                        })

                        return channelItem
                    })

                    self.twitchChannels = _twitchChannels

                    // res.sort(function (a, b) {
                    //     // console.log(a.sort);
                    //     // console.log(b.sort);
                    //     // return b.sort - a.sort;
                    //     if (item.ok) {
                    //         self.$http.get('https://api.twitch.tv/kraken/streams/' + item.name, {
                    //                 headers: {
                    //                     'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
                    //                 }
                    //             })
                    //             .then(function (response) {
                    //                 var stream = response.body.stream;
                    //                 item.stream = stream ? stream.channel.status : 'Offline';
                    //                 item.class = stream ? 'panel panel-success' : 'panel panel-default';
                    //                 item.sort = stream ? -1 : item.sort;
                    //                 self.twitchChannels.push(item)
                    //             })
                    //     } else {
                    //         item.stream = item.statusText;
                    //         self.twitchChannels.push(item)
                    //     }
                    // })
                    // return res;

                })
                
                .catch(function (err) {
                    console.log(err);
                });


        }
    }
})