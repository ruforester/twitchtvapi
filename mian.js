new Vue({
    el: '#app',
    data: {
        twitchChannels: [],
        userList: [
            "ESL_SC2",
            "OgamingSC2",
            "cretetion",
            "freecodecamp",
            "storbeck",
            "habathcx",
            "RobotCaleb",
            "noobs2ninjas",
            "comster404",
            "brunofin",
            "evilarthas",
            "nickbunyun"
        ]
    },
    created: function () {
        this.twitchChannels = this.fetchTwitchChannels(this.userList)
    },
    methods: {
        fetchTwitchChannels: function (userList) {
            const self = this

            const axiosListUser = userList.map((user) => {
                return axios.get('https://api.twitch.tv/kraken/channels/' + user, {
                    headers: {
                        'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
                    },
                    validateStatus: function (status) {
                        return (status >= 200 && status < 300) || status === 404
                    }
                })
            })

            const axiosListStream = userList.map((user) => {
                return axios.get('https://api.twitch.tv/kraken/streams/' + user, {
                    headers: {
                        'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy',
                        'Accept': 'application/vnd.twitchtv.v5+json'
                    },
                    validateStatus: function (status) {
                        return (status >= 200 && status < 300) || status === 404
                    }
                })
            })

            // const axiosListUser = axios.get('https://api.twitch.tv/kraken/channels/ESL_SC2', {
            //     headers: {
            //         'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
            //     },
            //     validateStatus: function (status) {
            //         return (status >= 200 && status < 300) || status === 404
            //     }
            // })

            // const axiosListStream = axios.get('https://api.twitch.tv/kraken/streams/ESL_SC2', {
            //     headers: {
            //         'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
            //     },
            //     validateStatus: function (status) {
            //         return (status >= 200 && status < 300) || status === 404
            //     }
            // })

            axios.all(axiosListStream)
                .then(axios.spread(function () {
                    console.log(arguments[0])
                }))


                //     return {
                //         userData: response,
                //         streamData: axiosListStream
                //     }
                // })
                // .then(function (object) {
                //     const streamData2 = axios.all(object.streamData)
                //     return {
                //         userData: object.userData,
                //         streamData2: streamData2
                //     }
                // })
                // .then(function (res) {
                //     console.log(res)
                // })
                .catch(function (error) {
                    console.log(error)
                })
        }
    }
})

function testAxios() {
    return axios.get('https://api.twitch.tv/kraken/channels/ESL_SC2', {
        headers: {
            'Client-ID': 'mqmnnsstd0xi4gw7rnjt89e755vawwy'
        }
    })
}
